package garden;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gridModel.GardenCell;
import gridModel.Coordinate;
import itemModel.Direction;
import itemModel.Mower;
import itemModel.MowerCommand;

public class Garden {
	private Map<Coordinate, GardenCell> gardenGrid;
	private List<Mower> mowerList = new ArrayList<>();

	@SuppressWarnings("unused")
	private Garden() {
	};

	public Garden(int width, int height) {
		gardenGrid = new HashMap<>();
		for (int x = 0; x <= width; x++) {
			for (int y = 0; y <= height; y++) {
				Coordinate cellCoordinate = new Coordinate(x, y);
				gardenGrid.put(cellCoordinate, new GardenCell(cellCoordinate));
			}
		}
	}

	private Boolean isEmptyCell(Coordinate coordinate) {
		return gardenGrid.containsKey(coordinate) && gardenGrid.get(coordinate).isEmpty();
	}

	public void createMower(int xCoordinate, int yCoordinate, Direction direction) {
		Coordinate staringPosition = new Coordinate(xCoordinate, yCoordinate);
		mowerList.add(new Mower(staringPosition, direction));
		gardenGrid.get(staringPosition).setIsEmpty(false);
	}

	public void moveLastMower(MowerCommand command) {
		moveMower(mowerList.get(mowerList.size() - 1), command);
	}

	public String getMowerCoordinatesAsString(int mowerIndex) throws Exception {
		if (mowerList.size() > mowerIndex) {
			Coordinate mowerCoordinate = mowerList.get(mowerIndex).getCoordinate();
			return Integer.toString(mowerCoordinate.getX()) + " " + Integer.toString(mowerCoordinate.getY()) + " "
					+ mowerList.get(mowerIndex).getDirection().name();
		}
		throw new Exception("The mower " + Integer.toString(mowerIndex) + " does not exist");
	}

	private void moveMower(Mower mower, MowerCommand command) {
		mower.updateDirection(command);
		if (command.equals(MowerCommand.A)) {
			Coordinate nextCoordinate = mower.getCoordinate().nextCoordinate(mower.getDirection());
			if (isEmptyCell(nextCoordinate)) {
				gardenGrid.get(mower.getCoordinate()).setIsEmpty(true);
				mower.setCoordinate(nextCoordinate);
				gardenGrid.get(mower.getCoordinate()).setIsEmpty(false);
			}
		}
	}

}
