package garden;

import java.util.Arrays;
import java.util.List;

import itemModel.Direction;
import itemModel.MowerCommand;

public class GardenWorld {

	private Garden garden;
	
	public Garden getGarden() {
		return this.garden;
	}

	public void createGarden(String sizeAsString) {
		List<String> gardenSize = Arrays.asList(sizeAsString.split(" "));
		if (gardenSize.size() == 2) {
			garden = new Garden(Integer.parseInt(gardenSize.get(0)), Integer.parseInt(gardenSize.get(1)));
		}
	}

	public void createMower(String mowerAttributesAsString) {
		if (garden != null) {
			List<String> mowerAttributes = Arrays.asList(mowerAttributesAsString.split(" "));
			if (mowerAttributes.size() == 3) {
				garden.createMower(Integer.parseInt(mowerAttributes.get(0)), Integer.parseInt(mowerAttributes.get(1)),
						Direction.valueOf(mowerAttributes.get(2)));
			}
		}
	}

	public void moveLastMower(String commandListAsString) {
		if (garden != null) {
			Arrays.asList(commandListAsString.split(""))
					.forEach(commandAsString -> garden.moveLastMower(MowerCommand.valueOf(commandAsString)));
		}

	}

}
