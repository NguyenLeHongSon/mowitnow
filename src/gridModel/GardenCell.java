package gridModel;

public class GardenCell {
	private Coordinate coordinate;
	private Boolean isEmpty = true;
	
	public GardenCell(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public Boolean isEmpty() {
		return isEmpty;
	}

	public void setIsEmpty(Boolean isEmpty) {
		this.isEmpty = isEmpty;
	}

}
