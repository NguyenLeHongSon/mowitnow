package test.java.garden;

import static org.junit.Assert.*;

import org.junit.Test;

import garden.GardenWorld;

public class GardenTest {

	@Test
	public void when_two_mowers_are_moved_then_their_final_positions_should_be_correct() throws Exception {
		GardenWorld gardenworld = new GardenWorld();
		gardenworld.createGarden("5 5");
		gardenworld.createMower("1 2 N");
		gardenworld.moveLastMower("GAGAGAGAA");
		assertEquals("1 3 N", gardenworld.getGarden().getMowerCoordinatesAsString(0));
		
		gardenworld.createMower("3 3 E");
		gardenworld.moveLastMower("AADAADADDA");
		assertEquals("5 1 E", gardenworld.getGarden().getMowerCoordinatesAsString(1));
	}

}
