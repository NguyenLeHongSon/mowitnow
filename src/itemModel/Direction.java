package itemModel;

public enum Direction {
	N,
	E,
	O,
	S;

	private Direction left;
	private Direction right;
	
	static {
		N.left = O;
		N.right = E;
		E.left = N;
		E.right = S;
		S.left = E;
		S.right = O;
		O.left = S;
		O.right = N;
	}
	
	public Direction getLeft() {
		return left;
	}

	public Direction getRight() {
		return right;
	}
	
	public Direction getNextDirection(MowerCommand mowerCommand) {
		if (mowerCommand.equals(MowerCommand.G)) {
			return this.left;
		} else if (mowerCommand.equals(MowerCommand.D)) {
			return this.right;
		}
		return this;
	}
}
