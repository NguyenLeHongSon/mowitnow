package itemModel;

import gridModel.Coordinate;

public class Mower {
	private Coordinate coordinate;
	private Direction direction;
	
	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

	public Direction getDirection() {
		return direction;
	}

	public Mower(Coordinate coordinate, Direction direction) {
		this.coordinate = coordinate;
		this.direction = direction;
	}
	
	public void updateDirection(MowerCommand command) {
		this.direction = this.direction.getNextDirection(command);
	}
	
}
